<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

// Resource
use App\Http\Controllers\RuanganController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\MejaController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AplikasiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Resource Khusus
Route::get('/', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/', [LoginController::class, 'authenticate'])->middleware('guest');
Route::post('/logout', [LoginController::class, 'logout'])->middleware('auth');

// Resource Ruangan
Route::resource('/ruangan', RuanganController::class)->middleware('admin');

// Resource Barang
Route::get('/barang/cetak', [BarangController::class, 'cetak'])->middleware('admin');
Route::get('/barang/Monitor', [BarangController::class, 'Monitor'])->middleware('admin');
Route::get('/barang/Pc', [BarangController::class, 'Pc'])->middleware('admin');
Route::get('/barang/Keyboard', [BarangController::class, 'Keyboard'])->middleware('admin');
Route::get('/barang/Mouse', [BarangController::class, 'Mouse'])->middleware('admin');
Route::resource('/barang', BarangController::class)->except('create');

// Resource Meja
Route::get('/meja/{ruangan}', [MejaController::class, 'create'])->middleware('admin');
Route::post('/meja/{ruangan}', [MejaController::class, 'store'])->middleware('admin');
Route::resource('/meja', MejaController::class)->except(['create', 'store'])->middleware('admin');

// Monitor
Route::get('/monitor/{meja}', [MejaController::class, 'monitor'])->middleware('admin');
Route::post('/monitor/{meja}', [MejaController::class, 'createmonitor'])->middleware('admin');
Route::post('/monitor/{barang}/edit', [MejaController::class, 'updateBarang'])->middleware('admin');

// Pc
Route::get('/pc/{meja}', [MejaController::class, 'pc'])->middleware('admin');
Route::post('/pc/{meja}', [MejaController::class, 'createpc'])->middleware('admin');
Route::post('/pc/{barang}/edit', [MejaController::class, 'updatepc'])->middleware('admin');

// Mouse
Route::get('/mouse/{meja}', [MejaController::class, 'mouse'])->middleware('admin');
Route::post('/mouse/{meja}', [MejaController::class, 'createmouse']);
Route::post('/mouse/{barang}/edit', [MejaController::class, 'updateBarang'])->middleware('admin');

// Keyboard
Route::get('/keyboard/{meja}', [MejaController::class, 'keyboard'])->middleware('admin');
Route::post('/keyboard/{meja}', [MejaController::class, 'createkeyboard']);
Route::post('/keyboard/{barang}/edit', [MejaController::class, 'updateBarang'])->middleware('admin');

// Aplikasi
Route::get('/aplikasion/{meja}', [MejaController::class, 'aplikasion']);
Route::post('/aplikasion/{pc}/edit', [MejaController::class, 'updateaplikasion']);

// Resource Pengguna
Route::get('/pengguna/{meja}', [PenggunaController::class, 'index'])->middleware('admin');
Route::get('/pengguna/{meja}/create', [PenggunaController::class, 'create'])->middleware('admin');
Route::resource('/pengguna', PenggunaController::class)->except(['index', 'create'])->middleware('admin');

// Resource User
Route::get('/user/cetak', [UserController::class, 'cetak'])->middleware('admin');
Route::resource('/user', UserController::class)->middleware('admin');

// Resource Aplikasi
Route::resource('/aplikasi', AplikasiController::class)->middleware('admin');