@extends('layouts.main')

@section('content')
<div class="col-lg-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Borrowed Items</h4>
                    <table class="table table-bordered text-center">
                      <thead>
                        <tr>
                          <th> Borrower Name </th>
						  <th> Returned Items </th>
                          <th> Borrower Date </th>
                          <th> Returned Date </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="table">
                          <td> 1 </td>
                          <td> Herman Beck </td>
                          <td> Photoshop </td>
                          <td> $ 77.99 </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
</div>
@endsection

