@extends('layouts.main')

@section('content')
<div class="col-lg-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Borrowed Items</h4>
                    <table class="table table-bordered text-center">
                      <thead>
                        <tr>
                          <th> Borrower Name </th>
                          <th> Borrower Date </th>
                          <th> Items Borrowed </th>
                          <th> Room </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="table">
                          <td> 1 </td>
                          <td> Herman Beck </td>
                          <td> Photoshop </td>
                          <td> $ 77.99 </td>
                          <td>
                          	<button type="button" class="btn btn-primary btn-icon-text">
                             	Return <i class="mdi mdi-arrow-right btn-icon-prepend"></i>
							</button>
						  </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
@endsection

