@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Pengguna</h4>
                    <form class="forms-sample" method="post" action="/pengguna/{{ $pengguna->id }}">
                      @method('put')
                      @csrf
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" value="{{ old('name', $pengguna->name) }}" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="John Markos" required>
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" value="{{ old('username', $pengguna->user->first()->username) }}" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="john.markos" required>
                        @error('username')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="type_user">Type</label>
                        <select name="type_user" class="form-control" id="type_user">
                          @if(old('type_user', $pengguna->user->first()->type_user) == '1')
                            <option value="1">Admin</option>
                            <option value="2">Staff</option>
                            <option value="3">Student</option>
                          @elseif(old('type_user', $pengguna->user->first()->type_user) == '2')
                            <option value="2">Staff</option>
                            <option value="3">Student</option>
                            <option value="1">Admin</option>
                          @else
                            <option value="3">Student</option>
                            <option value="1">Admin</option>
                            <option value="2">Staff</option>
                          @endif
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" value="{{ old('email', $pengguna->user->first()->email) }}" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="john@example.com" required>
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kelas_pengguna">Kelas</label>
                        <input type="text" value="{{ old('kelas_pengguna', $pengguna->kelas_pengguna) }}" name="kelas_pengguna" class="form-control @error('kelas_pengguna') is-invalid @enderror" id="kelas_pengguna" placeholder="12" required>
                        @error('kelas_pengguna')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="jurusan_pengguna">Jurusan</label>
                        <input type="text" value="{{ old('jurusan_pengguna', $pengguna->jurusan_pengguna) }}" name="jurusan_pengguna" class="form-control @error('jurusan_pengguna') is-invalid @enderror" id="jurusan_pengguna" placeholder="RPL" required>
                        @error('jurusan_pengguna')
                          <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="tlp_pengguna">Telepon</label>
                        <input type="text" value="{{ old('tlp_pengguna', $pengguna->tlp_pengguna) }}" name="tlp_pengguna" class="form-control @error('tlp_pengguna') is-invalid @enderror" id="tlp_pengguna" placeholder="+62 878-1276-7459" required>
                        @error('tlp_pengguna')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                            <button type="submit" class="btn btn-primary mr-2">Add <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                            <!-- <button type="submit" class="btn btn-danger">Cancel <i class="mdi mdi-close btn-icon-prepend"></i></button> -->
                    </form>
                  </div>
                </div>
              </div>
@endsection