@component('mail::message')
# {{ $data['title'] }}
 
Seseorang baru saja memasuki akun<br>
aplikasi Maintenance Lab anda. Jika ini<br>
memang Anda, Anda tidak perlu melakukan apa-apa.
 
@component('mail::button', ['url' => $data['url']])
Periksa Aktivitas
@endcomponent
 
Terimakasih,<br>
{{ config('app.name') }}
@endcomponent