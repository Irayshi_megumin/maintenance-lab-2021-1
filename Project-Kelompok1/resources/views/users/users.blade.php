@extends('layouts.main')

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body table-responsive">
                    <h4 class="card-title">All Users</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                      </ol>
                    </nav>
                    <a href="/user/create">
                        <button type="button" class="btn btn-primary btn-icon-text ml-2" style="float: right;">
                        <i class="mdi mdi-account-plus btn-icon-prepend"></i>	 Add Users
                        </button>
                    </a>
                    <a href="/user/cetak" target="_blank">
                      <button type="button" class="btn btn-warning btn-icon-text" style="float: right;">
                      <i class="mdi mdi mdi-printer btn-icon-prepend"></i> Print
                      </button>
                    </a>
                    @if(!$users->count())
                      <h5 class="text-center mt-2 mb-0">Tidak ada user, mohon tambahkan user!</h5>
                    @else
                    <table class="table table-hover mb-3">
                      <thead>
                        <tr>
                          <th> No </th>
						              <th> Name </th>
                          <th> Username </th>
                          <th> Type </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($users as $user)
                        <tr class="table">
                          <td>{{ $loop->iteration }}</td>
                          <td> {{ $user->pengguna->name }} </td>
                          <td> {{ $user->username }} </td>
                          <td>
                            <?php if($user->type_user == '1') {echo 'Admin';} elseif($user->type_user == '2') {echo 'Staff';} else {echo 'Siswa';} ?>
                          </td>
                          <td>
                          <!-- <button class="btn btn-outline-success">View <i class="mdi mdi-eye icon-large"></i></button> -->
                          <a href="/user/{{ $user->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                          <form action="/user/{{ $user->id }}" method="post" style="display: inline-block;">
                              @method('delete')
                              @csrf
                              <button class="btn btn-outline-danger" type="submit" onclick="return confirm('User ini akan dihapus secara permanen, apa kamu yakin?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                          </form>
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                    {{ $users->links() }}
                    @endif
                  </div>
          
@endsection
