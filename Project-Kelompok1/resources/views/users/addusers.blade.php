@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add User</h4>
                    <form class="forms-sample" method="post" action="/user">
                      @csrf
                      <div class="form-group">
                        <label for="name">Nama lengkap</label>
                        <input type="text" value="{{ old('name', 'John Markos') }}" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Contoh: John Markos" required>
                        @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kelas_pengguna">Kelas</label>
                        <input type="text" value="{{ old('kelas_pengguna', '12') }}" name="kelas_pengguna" class="form-control @error('kelas_pengguna') is-invalid @enderror" id="kelas_pengguna" placeholder="Contoh: 12" required>
                        @error('kelas_pengguna')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="jurusan_pengguna">Jurusan</label>
                        <input type="text" value="{{ old('jurusan_pengguna', 'RPL') }}" name="jurusan_pengguna" class="form-control @error('jurusan_pengguna') is-invalid @enderror" id="jurusan_pengguna" placeholder="Contoh: RPL / Rekayasa Perangkat Lunak" required>
                        @error('jurusan_pengguna')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="tlp_pengguna">Telepon</label>
                        <input type="text" value="{{ old('tlp_pengguna', '+62 878-1276-7459') }}" name="tlp_pengguna" class="form-control @error('tlp_pengguna') is-invalid @enderror" id="tlp_pengguna" placeholder="Contoh: +62 878-1276-7459" required>
                        @error('tlp_pengguna')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" value="{{ old('username', 'john.markos') }}" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Contoh: john.markos" required>
                        @error('username')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="text" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Contoh: Pa#sw0rd" required>
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="type_user">Type user</label>
                        <select name="type_user" class="form-control" id="type_user">
                        @if(old('type_user' == '1'))
                          <option value="1">Admin</option>
                          <option value="2">Staff</option>
                          <option value="3">Pengguna</option>
                        @elseif(old('type_user' == '2'))
                          <option value="2">Staff</option>
                          <option value="3">Pengguna</option>
                          <option value="1">Admin</option>
                        @else
                          <option value="3">Pengguna</option>
                          <option value="2">Staff</option>
                          <option value="1">Admin</option>
                        @endif
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" value="{{ old('email', 'john@gmail.com') }}" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Contoh: john@gmail.com" required>
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="meja_id">Pengguna meja</label>
                        <select name="meja_id" class="form-control" id="meja_id">
                        @if(old('meja_id'))
                          <option value="{{ old('meja_id') }}">{{ $mejas->find(old('meja_id'))->ruangan->nama_ruangan }} - {{ $mejas->find(old('meja_id'))->nama_meja }}</option>
                        @endif
                          <option value="{{ null }}">Tidak ada meja yang dipilih</option>
                        @foreach($mejas as $meja)
                        @if(old('meja_id') != $meja->id)
                          <option value="{{ $meja->id }}">{{  $meja->ruangan->nama_ruangan  }} - {{ $meja->nama_meja }}</option>
                        @endif
                        @endforeach
                        </select>
                      </div>
                          <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                          <a href="/user" class="btn btn-danger mr-2 float-right"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
@endsection