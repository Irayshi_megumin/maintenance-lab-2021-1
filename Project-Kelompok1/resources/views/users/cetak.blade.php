<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fabcart</title>
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/css/vendor.bundle.base.css') }}">

    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('templates/assets/css/style.css') }}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{ asset('templates/assets/images/favicon.png') }}" />
</head>
<body>

<div class="body-section">
            <br>
            <h4 class="card-title text-dark" style="margin-bottom: 20px;">List All Users</h4>
              @if(!$users->count())
                <h5 class="text-center mt-2 mb-0">Tidak ada user, mohon tambahkan user!</h5>
              @else
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th> Username </th>
                            <th> Type </th>
                            <th> Full Name </th>
                            <th> Kelas </th>
                            <th> Jurusan </th>
                            <th> No Telp </th>
                            <th> Email </th>
                            <th> CreatedAt </th>
                            <th> UpdatedAt </th>
                        </tr>
                    </thead>
                    <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td> {{ $user->username }} </td>
                        <td>
                          <?php if($user->type_user == '1') {echo 'Admin';} elseif($user->type_user == '2') {echo 'Staff';} else {echo 'Siswa';} ?>
                        </td>
                        <td> {{ $user->pengguna->name }} </td>
                        <td> {{ $user->pengguna->kelas_pengguna }} </td>
                        <td> {{ $user->pengguna->jurusan_pengguna }} </td>
                        <td> {{ $user->pengguna->tlp_pengguna }} </td>
                        <td> {{ $user->email }} </td>
                        <td> {{ $user->created_at }} </td>
                        <td> {{ $user->updated_at }} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            <br>
        </div>
        <script type="text/javascript">
          window.print();
        </script>
    </div>         
    <script src="{{ asset('templates/assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('templates/assets/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('templates/assets/vendors/jquery-circle-progress/js/circle-progress.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('templates/assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('templates/assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('templates/assets/js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('templates/assets/js/dashboard.js') }}"></script>
    <!-- End custom js for this page -->
</body>
</html>