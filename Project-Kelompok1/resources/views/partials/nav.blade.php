<!-- partial -->
<div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-category">Main</li>
            @can('admin')
            <li class="nav-item {{ request()->is('room') ? ' active' : '' }}">
              <a class="nav-link" href="/ruangan">
                <span class="icon-bg"><i class="mdi mdi-cube menu-icon"></i></span>
                <span class="menu-title">Home</span>
              </a>
            </li>
            <li class="nav-item {{ request()->is('users') ? ' active' : '' }}">
              <a class="nav-link" href="/user">
                <span class="icon-bg"><i class="mdi mdi-account menu-icon"></i></span>
                <span class="menu-title">Users</span>
              </a>
            </li>
            <li class="nav-item {{ request()->is('aplikasi') ? ' active' : '' }}">
              <a class="nav-link" href="/aplikasi">
                <span class="icon-bg"><i class="mdi mdi-application menu-icon"></i></span>
                <span class="menu-title">Aplikasi</span>
              </a>
            </li>
            <li class="nav-item {{ request()->is('barang') ? ' active' : '' }}">
              <a class="nav-link" href="/barang">
                <span class="icon-bg"><i class="mdi mdi-clipboard-text menu-icon"></i></span>
                <span class="menu-title">Barang</span>
              </a>
            </li>
            @endcan
            @can('users')
            <li class="nav-item {{ request()->is('barang') ? ' active' : '' }}">
              <a class="nav-link" href="/aplikasion/{{ auth()->user()->pengguna->meja_id }}">
                <span class="icon-bg"><i class="mdi mdi-format-list-bulleted menu-icon"></i></span>
                <span class="menu-title">Aplikasi</span>
              </a>
            </li>
            <li class="nav-item {{ request()->is('barang') ? ' active' : '' }}">
              <a class="nav-link" href="/barang">
                <span class="icon-bg"><i class="mdi mdi-format-list-bulleted menu-icon"></i></span>
                <span class="menu-title">Informasi Pc</span>
              </a>
            </li>
            @endcan
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="icon-bg"><i class="mdi mdi-crosshairs-gps menu-icon"></i></span>
                <span class="menu-title">Transaction</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item {{ request()->is('new') ? ' active' : '' }}"> <a class="nav-link" href="/new">New</a></li>
                  <li class="nav-item {{ request()->is('borrowed') ? ' active' : '' }}"> <a class="nav-link" href="/borrowed">Borrowed Items</a></li>
                  <li class="nav-item {{ request()->is('returned') ? ' active' : '' }}"> <a class="nav-link" href="/returned">Returned Items</a></li>
                </ul>
              </div>
            </li> -->
          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
			<div class="content-wrapper">
            	<div class="row" id="proBanner">
					@yield('content')
				</div>
			</div>
			
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>