@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4 class="card-title">Informasi Pc</h4>
                  <nav aria-label="breadcrumb" style="display: inline-block;">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="/ruangan">Ruangan</a></li>
                      <li class="breadcrumb-item"><a href="/ruangan/{{ $meja->ruangan_id }}">Meja</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Pc</li>
                    </ol>
                  </nav>
                    @if(!$pc)
                    <h5>Tidak ada pc, mohon tambahkan pc!</h5>
                    <form class="forms-sample" method="post" action="/pc/{{ $meja->id }}">
                    @csrf
                        <div class="form-group">
                        <label for="kapasitas_ssd">Kapasitas SSD</label>
                        <input type="text" value="{{ old('kapasitas_ssd', 'Kingston - 512GB') }}" name="kapasitas_ssd" class="form-control @error('kapasitas_ssd') is-invalid @enderror" id="kapasitas_ssd" placeholder="Contoh: Kingston - 512GB" required>
                        @error('kapasitas_ssd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_hdd">Kapasitas HDD</label>
                        <input type="text" value="{{ old('kapasitas_hdd', 'BarraCuda - 1TB') }}" name="kapasitas_hdd" class="form-control @error('kapasitas_hdd') is-invalid @enderror" id="kapasitas_hdd" placeholder="Contoh: BarraCuda - 1TB" required>
                        @error('kapasitas_hdd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_ram">Kapasitas Ram</label>
                        <input type="text" value="{{ old('kapasitas_ram', 'V-Gen - DDR3 - 2GB') }}" name="kapasitas_ram" class="form-control @error('kapasitas_ram') is-invalid @enderror" id="kapasitas_ram" placeholder="Contoh: V-Gen - DDR3 - 2GB" required>
                        @error('kapasitas_ram')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="processor">Processor</label>
                        <input type="text" value="{{ old('processor', 'Intel Core I7-870') }}" name="processor" class="form-control @error('processor') is-invalid @enderror" id="processor" placeholder="Contoh: Intel Core I7-870" required>
                        @error('processor')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-account-plus btn-icon-prepend"></i> Add Pc
                      </button>
                    </form>
                    @else
                    <form class="forms-sample" method="post" action="/pc/{{ $pc->id }}/edit">
                      @csrf
                      <div class="form-group">
                        <label for="kapasitas_ssd">Kapasitas SSD</label>
                        <input type="text" value="{{ old('kapasitas_ssd', $pc->pc->first()->kapasitas_ssd) }}" name="kapasitas_ssd" class="form-control @error('kapasitas_ssd') is-invalid @enderror" id="kapasitas_ssd" placeholder="Contoh: Kingston - 512GB" required>
                        @error('kapasitas_ssd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_hdd">Kapasitas HDD</label>
                        <input type="text" value="{{ old('kapasitas_hdd', $pc->pc->first()->kapasitas_hdd) }}" name="kapasitas_hdd" class="form-control @error('kapasitas_hdd') is-invalid @enderror" id="kapasitas_hdd" placeholder="Contoh: BarraCuda - 1TB" required>
                        @error('kapasitas_hdd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_ram">Kapasitas Ram</label>
                        <input type="text" value="{{ old('kapasitas_ram', $pc->pc->first()->kapasitas_ram) }}" name="kapasitas_ram" class="form-control @error('kapasitas_ram') is-invalid @enderror" id="kapasitas_ram" placeholder="Contoh: V-Gen - DDR3 - 2GB" required>
                        @error('kapasitas_ram')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="processor">Processor</label>
                        <input type="text" value="{{ old('processor', $pc->pc->first()->processor) }}" name="processor" class="form-control @error('processor') is-invalid @enderror" id="processor" placeholder="Contoh: Intel Core I7-870" required>
                        @error('processor')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="status_barang">Ubah Status</label>
                        <select name="status_barang" class="form-control" id="status_barang">
                        @if(old('status_barang', $pc->status_barang) == '1')
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                        @elseif(old('status_barang', $pc->status_barang) == '2')
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                        @elseif(old('status_barang', $pc->status_barang) == '3')
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                        @endif
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary float-right">Submit <i class="mdi mdi-check btn-icon-prepend"></i></button>
                      <a href="/ruangan/{{ $meja->ruangan_id }}" class="btn btn-danger mr-2 float-right"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                      <!-- <form action="/barang/{{ $pc->id }}" method="post" style="float: right;">
                        @method('delete')
                          @csrf
                          <button class="btn btn-danger mr-2" type="submit" onclick="return confirm('Ini akan menghapus pc secara permanen, yakin ingin menghapusnya?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                      </form> -->
                    </form>
                    @endif
                  </div>
                </div>
              </div>
@endsection