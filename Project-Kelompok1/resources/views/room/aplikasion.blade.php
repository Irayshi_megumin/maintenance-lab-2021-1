@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">List Status Aplikasi</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/ruangan">Ruangan</a></li>
                        <li class="breadcrumb-item"><a href="/ruangan/{{ $back }}">Meja</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Monitor</li>
                      </ol>
                    </nav>
                    @if(!$aplikasis->count())
                    <h5>Tidak ada aplikasi yang perlu diinstal, tambahkan aplikasi?</h5>
                    <a href="/additems">
                      <button type="button" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-account-plus btn-icon-prepend"></i> Add Aplikasi
                      </button>
                    </a>
                    @else
                    <form class="forms-sample" method="post" action="/aplikasion/{{ $status_aplikasis->first()->pc_id }}/edit">
                      @csrf
                      <table class="table table-hover mb-3">
                        <thead>
                          <tr>
                            <th>Nama Aplikasi</th>
                            <th>Status Aplikasi</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($aplikasis as $aplikasi)
                          <tr class="table">
                            <td>{{ $aplikasi->nama_aplikasi }}</td>
                            <td>
                              <div class="form-group">
                                <select name="status_aplikasi[]" class="form-control" id="status_aplikasi">
                                @if(old('status_aplikasi', $status_aplikasis[$loop->index]->status_aplikasi) == '1')
                                  <option value="1">Belum Terinstal</option>
                                  <option value="2">Terinstal</option>
                                @elseif(old('status_aplikasi', $status_aplikasis[$loop->index]->status_aplikasi) == '2')
                                  <option value="2">Terinstal</option>
                                  <option value="1">Belum Terinstal</option>
                                @endif
                                </select>
                              </div>
                              <input type="hidden" name="aplikasi_id[]" value="{{ $status_aplikasis[$loop->index]->aplikasi_id }}">
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                    <a href="/ruangan/{{ $back }}" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                    @endif
    </div>
          
@endsection
