@extends('layouts.main')

@section('content')
<div class="col-lg-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                  <h4 class="card-title">Ruangan</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Ruangan</li>
                      </ol>
                    </nav>
                    <a href="/ruangan/create">
                      <button type="button" class="btn btn-primary btn-icon-text ml-2" style="float: right;">
                      <i class="mdi mdi-application btn-icon-prepend"></i> Add Room
                      </button>
                    </a>	
                    <table class="table text-center table-hover">
                      <thead>
                        <tr>
                          <th> Room Name </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($rooms as $room)
                        <tr>
                          <td>{{ $room->nama_ruangan }}</td>
                          <td> 
                            <a href="/ruangan/{{ $room->id }}">
                              <button type="button" class="btn btn-outline-success btn-icon-text">
                              <i class="mdi mdi-eye btn-icon-prepend"></i> View
                              </button>
                            </a>
                            <a href="/ruangan/{{ $room->id }}/edit">
                              <button type="button" class="btn btn-outline-primary btn-icon-text">
                              <i class="mdi mdi-scale-bathroom btn-icon-prepend"></i> Edit
                              </button>
                            </a>	
                            <form action="/ruangan/{{ $room->id }}" method="post" style="display: inline-block;">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-outline-danger btn-icon-text"  onclick="return confirm('Yakin akan menghapus seluruh data dari Ruangan ini secara permanen?')"><i class="mdi mdi-cup icon-large"></i> Delete</button>
                            </form>
                            </div>
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>      
@endsection