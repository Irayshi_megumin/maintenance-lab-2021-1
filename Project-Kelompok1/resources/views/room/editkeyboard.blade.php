@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Informasi Keyboard</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/ruangan">Ruangan</a></li>
                        <li class="breadcrumb-item"><a href="/ruangan/{{ $meja->ruangan_id }}">Meja</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Keyboard</li>
                      </ol>
                    </nav>
                    @if(!$keyboard)
                    <form class="forms-sample" method="post" action="/keyboard/{{ $meja->id }}">
                    @csrf
                      <button type="submit" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-keyboard btn-icon-prepend"></i> Add Keyboard
                      </button>
                    </form>
                    <h5>Tidak ada keyboard, mohon tambahkan keyboard!</h5>
                    @else
                    <form class="forms-sample" method="post" action="/keyboard/{{ $keyboard->id }}/edit">
                      @csrf
                      <div class="form-group">
                        <label for="status_barang">Ubah Status</label>
                        <select name="status_barang" class="form-control" id="status_barang">
                        @if(old('status_barang', $keyboard->status_barang) == '1')
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                        @elseif(old('status_barang', $keyboard->status_barang) == '2')
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                        @elseif(old('status_barang', $keyboard->status_barang) == '3')
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                        @endif
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                      <a href="/ruangan/{{ $meja->ruangan->id }}" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                      <!-- <form action="/barang/{{ $keyboard->id }}" method="post" style="display: inline-block;">
                        @method('delete')
                          @csrf
                          <button class="btn btn-danger mr-2" type="submit" onclick="return confirm('Ini akan menghapus keyboard secara permanen, yakin ingin menghapusnya?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                      </form> -->
                    </form>
                    @endif
                  </div>
                </div>
              </div>
@endsection