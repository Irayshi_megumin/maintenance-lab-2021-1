@extends('layouts.main')

@section('content')
<div class="col-lg-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title" style="display: inline-block;">Detail Pc</h4>
                      <a href="/editpc">
                        <button type="button" class="btn btn-primary btn-icon-text" style="float: right;">
                        <i class="mdi mdi-account-plus btn-icon-prepend"></i> Edit Pc
                        </button>
                      </a>
                    <table class="table table-bordered text-center">
                      <thead class="table-dark">
                        <tr>
                          <th> Kapasitas Ssd </th>
					            	  <th> Kapasitas Hdd </th>
                          <th> Kapasitas Ram </th>
                          <th> Processor </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="table">
                          <td> Lorem </td>
                          <td> Lorem </td>
                          <td> Lorem </td>
                          <td> Lorem </td>
                        </tr>
                      </tbody>
                    </table>
    </div>
          
@endsection
