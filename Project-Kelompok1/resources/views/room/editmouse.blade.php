@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Informasi Mouse</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/ruangan">Ruangan</a></li>
                        <li class="breadcrumb-item"><a href="/ruangan/{{ $meja->ruangan_id }}">Meja</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Mouse</li>
                      </ol>
                    </nav>
                    @if(!$mouse)
                    <form class="forms-sample" method="post" action="/mouse/{{ $meja->id }}">
                    @csrf
                      <button type="submit" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-mouse btn-icon-prepend"></i> Add Mouse
                      </button>
                    </form>
                    <h5>Tidak ada mouse, mohon tambahkan mouse!</h5>
                    @else
                    <form class="forms-sample" method="post" action="/mouse/{{ $mouse->id }}/edit">
                      @csrf
                      <div class="form-group">
                        <label for="status_barang">Ubah Status</label>
                        <select name="status_barang" class="form-control" id="status_barang">
                        @if(old('status_barang', $mouse->status_barang) == '1')
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                        @elseif(old('status_barang', $mouse->status_barang) == '2')
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                        @elseif(old('status_barang', $mouse->status_barang) == '3')
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                        @endif
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary float-right">Submit <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                      <a href="/ruangan/{{ $meja->ruangan->id }}" class="btn btn-danger mr-2 float-right">Cancel <i class="mdi mdi-close btn-icon-prepend"></i></a>
                      <!-- <form action="/barang/{{ $mouse->id }}" method="post" style="float: right;">
                        @method('delete')
                          @csrf
                          <button class="btn btn-danger mr-2" type="submit" onclick="return confirm('Ini akan menghapus mouse secara permanen, yakin ingin menghapusnya?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                      </form> -->
                    </form>
                    @endif
                  </div>
                </div>
              </div>
@endsection