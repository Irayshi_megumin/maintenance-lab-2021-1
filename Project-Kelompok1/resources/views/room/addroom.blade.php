@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Room</h4>
                    <form class="forms-sample" method="post" action="/ruangan">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputName1">Nama Room</label>
                        <input type="text" value="{{ old('nama_ruangan', 'Ruang 1') }}" name="nama_ruangan" class="form-control @error('nama_ruangan') is-invalid @enderror" id="nama_ruangan" placeholder="Contoh: Ruang 1" required>
                        @error('nama_ruangan')
                          <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                            <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                            <a href="/ruangan" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
@endsection