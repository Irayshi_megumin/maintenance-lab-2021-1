@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Informasi Monitor</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/ruangan">Ruangan</a></li>
                        <li class="breadcrumb-item"><a href="/ruangan/{{ $meja->ruangan_id }}">Meja</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Monitor</li>
                      </ol>
                    </nav>
                    @if(!$monitor)
                    <form class="forms-sample" method="post" action="/monitor/{{ $meja->id }}">
                    @csrf
                      <button type="submit" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-monitor btn-icon-prepend"></i> Add Monitor
                      </button>
                    </form>
                    <h5>Tidak ada monitor, mohon tambahkan monitor!</h5>
                    @else
                    <form class="forms-sample" method="post" action="/monitor/{{ $monitor->id }}/edit">
                      @csrf
                      <div class="form-group">
                        <label for="status_barang">Ubah Status</label>
                        <select name="status_barang" class="form-control" id="status_barang">
                        @if(old('status_barang', $monitor->status_barang) == '1')
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                        @elseif(old('status_barang', $monitor->status_barang) == '2')
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                        @elseif(old('status_barang', $monitor->status_barang) == '3')
                          <option value="3">Hilang</option>
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                        @endif
                        </select>
                      </div>
                      <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                      <a href="/ruangan/{{ $meja->ruangan->id }}" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                    <!-- <form action="/barang/{{ $monitor->id }}" method="post" style="float: right;">
                      @method('delete')
                        @csrf
                        <button class="btn btn-danger mr-2" type="submit" onclick="return confirm('Ini akan menghapus monitor secara permanen, yakin ingin menghapusnya?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                    </form> -->
                    @endif
                  </div>
                </div>
              </div>
@endsection