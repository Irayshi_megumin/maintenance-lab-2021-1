@extends('layouts.main')

@section('content')
</style>
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body table-responsive">
                    <h4 class="card-title">Meja</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Ruangan</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Meja</li>
                      </ol>
                    </nav>
                    <a href="/meja/{{ $ruangan->id }}">
                      <button type="button" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-chair-school btn-icon-prepend"></i> Add Meja
                      </button>
                    </a>
                    @if($mejas->count() == null)
                      <h5>Tidak ada meja, mohon tambahkan meja!</h5>
                    @else
                    <table class="table table-hover mb-3">
                      <thead>
                          <th> No</th>
                          <th> Nama Meja </th>
						              <th> Pengguna </th>
                          <th> Monitor </th>
                          <th> Pc </th>
                          <th> Mouse </th>
                          <th> Keyboard </th>
                          <th> Aplikasi </th>
                          <th> Aksi </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($mejas as $meja)
                          <tr class="table">
                            <td>{{ $loop->iteration }}</td>
                            <td><a href="/meja/{{ $meja->id }}/edit" class="text-black">{{ $meja->nama_meja }}</a></td>
                            <td><a href="/pengguna/{{ $meja->id }}">{!! ($meja->pengguna->count()) ? '<label class="badge badge-success">Ada</label>' : '-' !!}</a></td>
                            
                            <?php $meja_status_monitor = 0; ?>
                            @foreach($meja->barang->where('nama_barang', 'Monitor') as $meja_barang)
                              <?php $meja_status_monitor = $meja_barang->status_barang ?>
                            @endforeach
                            <td><a href="/monitor/{{ $meja->id }}"><label class="badge @if($meja_status_monitor == '1') badge-success @elseif($meja_status_monitor == '2') badge-info @elseif($meja_status_monitor == '3') badge-danger @else  @endif"><?php if($meja_status_monitor == '1') {echo 'Normal';} elseif($meja_status_monitor == '2') {echo 'Rusak';} elseif($meja_status_monitor == '3') {echo 'Hilang';} else {echo '-';} ?></label></a></td>

                            <?php $meja_status_pc = 0; ?>
                            @foreach($meja->barang->where('nama_barang', 'Pc') as $meja_barang)
                              <?php $meja_status_pc = $meja_barang->status_barang ?>
                            @endforeach
                            <td><a href="/pc/{{ $meja->id }}"><label class="badge @if($meja_status_pc == '1') badge-success @elseif($meja_status_pc == '2') badge-info @elseif($meja_status_pc == '3') badge-danger @else  @endif"><?php if($meja_status_pc == '1') {echo 'Normal';} elseif($meja_status_pc == '2') {echo 'Rusak';} elseif($meja_status_pc == '3') {echo 'Hilang';} else {echo '-';} ?></label></a></td>
                            
                            <?php $meja_status_mouse = 0; ?>
                            @foreach($meja->barang->where('nama_barang', 'Mouse') as $meja_barang)
                              <?php $meja_status_mouse = $meja_barang->status_barang ?>
                            @endforeach
                            <td><a href="/mouse/{{ $meja->id }}"><label class="badge @if($meja_status_mouse == '1') badge-success @elseif($meja_status_mouse == '2') badge-info @elseif($meja_status_mouse == '3') badge-danger @else  @endif"><?php if($meja_status_mouse == '1') {echo 'Normal';} elseif($meja_status_mouse == '2') {echo 'Rusak';} elseif($meja_status_mouse == '3') {echo 'Hilang';} else {echo '-';} ?></label></a></td>
                            
                            <?php $meja_status_keyboard = 0; ?>
                            @foreach($meja->barang->where('nama_barang', 'Keyboard') as $meja_barang)
                              <?php $meja_status_keyboard = $meja_barang->status_barang ?>
                            @endforeach
                            <td><a href="/keyboard/{{ $meja->id }}"><label class="badge @if($meja_status_keyboard == '1') badge-success @elseif($meja_status_keyboard == '2') badge-info @elseif($meja_status_keyboard == '3') badge-danger @else  @endif"><?php if($meja_status_keyboard == '1') {echo 'Normal';} elseif($meja_status_keyboard == '2') {echo 'Rusak';} elseif($meja_status_keyboard == '3') {echo 'Hilang';} else {echo '-';} ?></label></a></td>

                            <?php $meja_status_aplikasi = 1; ?>
                            @foreach($meja->barang->where('nama_barang', 'Pc') as $meja_barang)
                              @foreach($meja_barang->pc as $meja_barang_pc)
                                  <?php $meja_status_aplikasi = $meja_barang_pc->status_aplikasi->where('status_aplikasi', '1')->count() ?>
                              @endforeach
                            @endforeach
                            <td>@if($meja_status_pc)<a href="/aplikasion/{{ $meja->id }}"><label class="badge @if($meja_status_aplikasi == '0') badge-success @else badge-danger @endif"><?php if($meja_status_aplikasi == '0') {echo 'Terinstal';} else {echo 'Belum Terinstal';} ?></label></a>@else <label class="">-</label> @endif</td>
                            <td>
                              <a href="/meja/{{ $meja->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                              <form action="/meja/{{ $meja->id }}" method="post" style="display: inline-block;">
                                  @method('delete')
                                  @csrf
                                  <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin akan menghapus seluruh data dari Meja ini secara permanen?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    @endif
                    <a href="/ruangan" class="btn btn-danger float-right"><i class="mdi mdi-close btn-icon-prepend"></i> Kembali</a>
                  </div>
          
@endsection
