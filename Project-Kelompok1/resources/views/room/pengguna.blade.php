@extends('layouts.main')

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body table-responsive">
                    <h4 class="card-title">List Pengguna {{ $meja->nama_meja }}</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/ruangan">Ruangan</a></li>
                        <li class="breadcrumb-item"><a href="/ruangan/{{ $meja->ruangan_id }}">Meja</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pengguna</li>
                      </ol>
                    </nav>
                    <a href="/pengguna/{{ $meja->id }}/create">
                      <button type="button" class="btn btn-primary btn-icon-text" style="float: right;">
                      <i class="mdi mdi-account-plus btn-icon-prepend"></i> Add Pengguna
                      </button>
                    </a>
                    @if(!$meja->pengguna->count())
                      <h5 class="text-center mt-2 mb-0">Tidak ada pengguna, mohon tambahkan pengguna!</h5>
                    @else
                    <table class="table table-hover mb-3">
                      <thead>
                        <tr>
                          <th> No </th>
                          <th> Full Name </th>
                          <th> Kelas </th>
                          <th> Jurusan </th>
                          <th> No Telp </th>
                          <th> Email </th>
                          <th> Aksi </th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($meja->pengguna as $pengguna)
                        <tr class="table">
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $pengguna->name }}</td>
                          <td>{{ $pengguna->kelas_pengguna }}</td>
                          <td>{{ $pengguna->jurusan_pengguna }}</td>
                          <td>{{ $pengguna->tlp_pengguna }}</td>
                          <td>{{ $pengguna->user->first()->email }}</td>
                          <td> 
                          <button class="btn btn-outline-success">View <i class="mdi mdi-eye icon-large"></i></button>
                          <a href="/pengguna/{{ $pengguna->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                          <form action="/pengguna/{{ $pengguna->id }}" method="post" style="display: inline-block;">
                              @method('delete')
                              @csrf
                              <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus pengguna dari meja ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                          </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  @endif
                  <a href="/ruangan/{{ $meja->ruangan_id }}" class="btn btn-danger float-right"><i class="mdi mdi-close btn-icon-prepend"></i> Kembali</a>
</div>          
@endsection
