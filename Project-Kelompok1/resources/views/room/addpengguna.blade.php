@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Pengguna</h4>
                    @if($penggunas->where('meja_id', null)->count() == null)
                      <h5>Semua pengguna sudah memiliki meja</h5>
                    @else
                    <form class="forms-sample" method="post" action="/pengguna">
                      @csrf
                      <div class="form-group">
                        <label for="pengguna">Pilih pengguna</label>
                        <select name="pengguna" class="form-control" id="pengguna">
                        @foreach($penggunas->where('meja_id', null) as $pengguna)
                          <option value="{{ $pengguna->id }}">{{ $pengguna->name }}</option>
                        @endforeach
                        </select>
                      </div>
                      <input type="hidden" name="meja_id" value="{{ $meja_id }}">
                      <button type="submit" class="btn btn-primary mr-2"> Add <i class="mdi mdi-arrow-right btn-icon-prepend"></i></button>
                      <!-- <button type="submit" class="btn btn-danger">Cancel <i class="mdi mdi-close btn-icon-prepend"></i></button> -->
                    </form>
                    @endif
                  </div>
                </div>
              </div>
@endsection