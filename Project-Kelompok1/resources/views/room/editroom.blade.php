@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Room</h4>
                    <form class="forms-sample" method="POST" action="/ruangan/{{ $ruangan->id }}">
                      @method('put')
                      @csrf
                      <div class="form-group">
                        <label for="nama_ruangan">Nama ruangan</label>
                        <input type="text" value="{{ $ruangan->nama_ruangan }}" name="nama_ruangan" class="form-control @error('nama_ruangan') is-invalid @enderror" id="nama_ruangan" placeholder="Contoh: Ruang 1" required>
                        @error('nama_ruangan')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                            <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                            <a href="/ruangan" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
@endsection