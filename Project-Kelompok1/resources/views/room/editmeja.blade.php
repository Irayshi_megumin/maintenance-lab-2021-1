@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Informasi Meja</h4>
                    <form class="forms-sample" method="post" action="/meja/{{ $meja->id }}">
                      @method('put')
                      @csrf
                      <div class="form-group">
                        <label for="nama_meja">Nama Meja</label>
                        <input type="text" value="{{ old('nama_meja', $meja->nama_meja) }}" name="nama_meja" class="form-control @error('nama_meja') is-invalid @enderror" id="nama_meja" placeholder="V-Gen - DDR3 - 2GB" required>
                        @error('nama_meja')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                      <a href="/ruangan/{{ $meja->ruangan_id }}" class="btn btn-danger mr-2 float-right"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
@endsection