@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Barang</h4>
                    <form class="forms-sample" method="post" action="/barang">
                      @csrf
                      <?php $list_nama_meja = []; $list_id_meja = []; $list_ruangan = []; ?>
                      @foreach($mejas as $meja)
                      <?php
                        if ($meja->barang->where('nama_barang', $barangs)->all() == null) {
                          $list_id_meja[] = $meja->id;
                          $list_nama_meja[] = $meja->nama_meja;
                          $list_ruangan[] = $meja->ruangan->nama_ruangan;
                        }
                      ?>
                      @endforeach
                      <div class="form-group">
                        <label for="meja_id">Pilih meja kosong dibawah!</label>
                        <select name="meja_id" class="form-control" id="meja_id">
                        @for ($i = 0; $i < count($list_id_meja); $i++)
                          <option value="{{ $list_id_meja[$i] }}">{{  $list_ruangan[$i]  }} - {{ $list_nama_meja[$i] }}</option>
                        @endfor
                        </select>
                      </div>
                      <input type="hidden" name="nama_barang" value="{{ $barangs }}">
                      <input type="hidden" name="status_barang" value="1">
                      
                      @if($barangs == 'Pc')
                      <div class="form-group">
                        <label for="kapasitas_ssd">Kapasitas SSD</label>
                        <input type="text" value="{{ old('kapasitas_ssd', 'Kingston - 512GB') }}" name="kapasitas_ssd" class="form-control @error('kapasitas_ssd') is-invalid @enderror" id="kapasitas_ssd" placeholder="Contoh: Kingston - 512GB" required>
                        @error('kapasitas_ssd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_hdd">Kapasitas HDD</label>
                        <input type="text" value="{{ old('kapasitas_hdd', 'BarraCuda - 1TB') }}" name="kapasitas_hdd" class="form-control @error('kapasitas_hdd') is-invalid @enderror" id="kapasitas_hdd" placeholder="Contoh: BarraCuda - 1TB" required>
                        @error('kapasitas_hdd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_ram">Kapasitas Ram</label>
                        <input type="text" value="{{ old('kapasitas_ram', 'V-Gen - DDR3 - 2GB') }}" name="kapasitas_ram" class="form-control @error('kapasitas_ram') is-invalid @enderror" id="kapasitas_ram" placeholder="Contoh: V-Gen - DDR3 - 2GB" required>
                        @error('kapasitas_ram')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="processor">Processor</label>
                        <input type="text" value="{{ old('processor', 'Intel Core I7-870') }}" name="processor" class="form-control @error('processor') is-invalid @enderror" id="processor" placeholder="Contoh: Intel Core I7-870" required>
                        @error('processor')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      @endif
                      <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                      <a href="/barang" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                  </div>
                </div>
              </div>

              <!-- <script>
                const barang = document.querySelector('#nama_barang');
                const meja = document.querySelector('#meja_id');

                barang.addEventListener('change', function() {
                  fetch('/addbarang/meja?barang=' + barang.value)
                    .then(response => response.json())
                    .then(data => meja.value = data.meja)
                });
              </script> -->
@endsection