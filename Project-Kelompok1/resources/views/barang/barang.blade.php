@extends('layouts.main')

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title" style="display: inline-block;">Barang</h4>
                    @can('admin')
                    <div class="d-sm-flex justify-content-between align-items-center transaparent-tab-border">
                      <ul class="nav nav-tabs tab-transparent" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-selected="true">All</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#monitor" role="tab" aria-selected="false">Monitor</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#pc" role="tab" aria-selected="false">Pc</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#mouse" role="tab" aria-selected="false">Mouse</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#keyboard" role="tab" aria-selected="false">Keyboard</a>
                        </li>
                      </ul>
                      <div class="d-md-block d-none">
                          <a href="/barang/cetak" target="_blank">
                            <button type="button" class="btn btn-warning btn-icon-text">
                              <i class="mdi mdi mdi-printer btn-icon-prepend"></i> Print
                            </button>
                          </a>
                          <button class="btn btn-primary btn-icon-text dropdown-toggle" type="button" id="dropdownMenuTambah" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-clipboard-text btn-icon-prepend"></i> Add Barang
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuTambah">
                            <a class="dropdown-item mdi mdi mdi-monitor" href="/barang/Monitor">Monitor</a>
                            <a class="dropdown-item mdi mdi-battery" href="/barang/Pc">Pc</a>
                            <a class="dropdown-item mdi mdi-mouse" href="/barang/Mouse">Mouse</a>
                            <a class="dropdown-item mdi mdi-keyboard" href="/barang/Keyboard">Keyboard</a>
                          </div>
                      </div>
                    </div>
                    @endcan
                    @if($barangs->count() == null)
                      <h5>Tidak ada barang, mohon tambahkan barang!</h5>
                    @else
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade show active" id="all" aria-labelledby="all">
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($barangs->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($barangs as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td> 
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                       {{  $barangs->links() }}
                      </div>
                      <div role="tabpanel" class="tab-pane fade overflow-auto" id="monitor" style="height: 50vh;">
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($monitors->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($monitors as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td> 
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                      </div>
                      <div role="tabpanel" class="tab-pane fade overflow-auto" id="pc" style="height: 50vh;">
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                              <th> Kapasitas SSD </th>
                              <th> Kapasitas HDD </th>
                              <th> Kapasitas Ram </th>
                              <th> Processor </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($pcs->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ssd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_hdd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ram }}</td>
                              <td>{{ $barang->pc->first()->processor }}</td>
                              <td>
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($pcs as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ssd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_hdd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ram }}</td>
                              <td>{{ $barang->pc->first()->processor }}</td>
                              <td> 
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                      </div>
                      <div role="tabpanel" class="tab-pane fade overflow-auto" id="mouse" style="height: 50vh;">
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($mouses->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($mouses as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td> 
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                      </div>
                      <div role="tabpanel" class="tab-pane fade overflow-auto" id="keyboard" style="height: 50vh;">
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($keyboards->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($keyboards as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td> 
                                <a href="/barang/{{ $barang->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                                <form action="/barang/{{ $barang->id }}" method="post" style="display: inline-block;">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus bbarang ini?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                      </div>
                    </div>
                    @endif
                  </div>
          
@endsection
