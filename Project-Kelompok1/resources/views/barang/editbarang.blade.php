@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Barang</h4>
                    <form class="forms-sample" method="post" action="/barang/{{ $barang->id }}">
                      @method('put')
                      @csrf
                      <?php $list_nama_meja = []; $list_id_meja = []; $list_ruangan = []; ?>
                      @foreach($mejas as $meja)
                      <?php
                        if ($meja->barang->where('nama_barang', $barang->nama_barang)->all() == null) {
                          $list_id_meja[] = $meja->id;
                          $list_nama_meja[] = $meja->nama_meja;
                          $list_ruangan[] = $meja->ruangan->nama_ruangan;
                        }
                      ?>
                      @endforeach
                      <div class="form-group">
                        <label for="meja_id">Pilih meja kosong dibawah!</label>
                        <select name="meja_id" class="form-control" id="meja_id">
                        @if($barang->meja_id))
                          <option value="{{ $barang->meja_id }}">
                              {{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}
                          </option>
                        @endif
                        @for ($i = 0; $i < count($list_id_meja); $i++)
                          <option value="{{ $list_id_meja[$i] }}">{{  $list_ruangan[$i]  }} - {{ $list_nama_meja[$i] }}</option>
                        @endfor
                        </select>
                      </div>
                      @can('admin')
                      <div class="form-group">
                        <label for="status_barang">Ubah Status</label>
                        <select name="status_barang" class="form-control" id="status_barang">
                        @if(old('status_barang', $barang->status_barang) == '1')
                          <option value="1">Normal</option>
                          <option value="2">Rusak</option>
                          <option value="3">Hilang</option>
                        @elseif(old('status_barang', $barang->status_barang) == '2')
                        <option value="2">Rusak</option>
                        <option value="3">Hilang</option>
                        <option value="1">Normal</option>
                        @else
                        <option value="3">Hilang</option>
                        <option value="2">Rusak</option>
                        <option value="1">Normal</option>
                        @endif
                        </select>
                      </div>
                      @endcan
                      <input type="hidden" name="nama_barang" value="{{ $barang->nama_barang }}">
                      
                      @if($barang->nama_barang == 'Pc')
                      <div class="form-group">
                        <label for="kapasitas_ssd">Kapasitas SSD</label>
                        <input type="text" value="{{ old('kapasitas_ssd', $barang->pc->first()->kapasitas_ssd) }}" name="kapasitas_ssd" class="form-control @error('kapasitas_ssd') is-invalid @enderror" id="kapasitas_ssd" placeholder="Kingston - 512GB" required>
                        @error('kapasitas_ssd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_hdd">Kapasitas HDD</label>
                        <input type="text" value="{{ old('kapasitas_hdd', $barang->pc->first()->kapasitas_hdd) }}" name="kapasitas_hdd" class="form-control @error('kapasitas_hdd') is-invalid @enderror" id="kapasitas_hdd" placeholder="BarraCuda - 1TB" required>
                        @error('kapasitas_hdd')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="kapasitas_ram">Kapasitas Ram</label>
                        <input type="text" value="{{ old('kapasitas_ram', $barang->pc->first()->kapasitas_ram) }}" name="kapasitas_ram" class="form-control @error('kapasitas_ram') is-invalid @enderror" id="kapasitas_ram" placeholder="V-Gen - DDR3 - 2GB" required>
                        @error('kapasitas_ram')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="processor">Processor</label>
                        <input type="text" value="{{ old('processor', $barang->pc->first()->processor) }}" name="processor" class="form-control @error('processor') is-invalid @enderror" id="processor" placeholder="Intel Core I7-870" required>
                        @error('processor')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      @endif
                      <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                      <a href="/barang"><button type="submit" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</button></a>
                    </form>
                  </div>
                </div>
              </div>

              <!-- <script>
                const barang = document.querySelector('#nama_barang');
                const meja = document.querySelector('#meja_id');

                barang.addEventListener('change', function() {
                  fetch('/addbarang/meja?barang=' + barang.value)
                    .then(response => response.json())
                    .then(data => meja.value = data.meja)
                });
              </script> -->
@endsection