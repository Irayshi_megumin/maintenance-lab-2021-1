<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fabcart</title>
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/css/vendor.bundle.base.css') }}">

    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('templates/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('templates/assets/css/style.css') }}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{ asset('templates/assets/images/favicon.png') }}" />
</head>
<body>

<div class="body-section">
            <br>
            <h4 class="card-title text-dark" style="margin-bottom: 20px; margin-left: 20px;">List Details Monitor</h4>
                @if($barangs->count() == null)
                    <h5>Tidak ada barang, mohon tambahkan barang!</h5>
                @else
                <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($monitors->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($monitors as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                        <h4 class="card-title text-dark" style="margin-bottom: 20px; margin-left: 20px;">List Detais Pc</h4>
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                              <th> Kapasitas SSD </th>
                              <th> Kapasitas HDD </th>
                              <th> Kapasitas Ram </th>
                              <th> Processor </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($pcs->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ssd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_hdd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ram }}</td>
                              <td>{{ $barang->pc->first()->processor }}</td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($pcs as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ssd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_hdd }}</td>
                              <td>{{ $barang->pc->first()->kapasitas_ram }}</td>
                              <td>{{ $barang->pc->first()->processor }}</td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                        <h4 class="card-title text-dark" style="margin-bottom: 20px; margin-left: 20px;">List Details Mouse</h4>
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($mouses->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($mouses as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
                        <h4 class="card-title text-dark" style="margin-bottom: 20px; margin-left: 20px;">List Details Keyboard</h4>
                        <table class="table table-hover mb-3">
                          <thead>
                            <tr>
                              <th> No </th>
                              <th> Nama </th>
                              <th> Status </th>
                              <th> Meja </th>
                            </tr>
                          </thead>
                          <tbody>
                            @can('users')
                            @foreach($keyboards->where('meja_id', auth()->user()->pengguna->meja_id) as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                            </tr>
                            @endforeach
                            @endcan()
                            @can('admin')
                            @foreach($keyboards as $barang)
                            <tr class="table">
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $barang->nama_barang }}</td>
                              <td>@if($barang->status_barang == '1') {{ 'Normal' }}
                                @elseif($barang->status_barang == '2') {{ 'Rusak' }}
                                @elseif($barang->status_barang == '3') {{ 'Hilang' }}
                                @else {{ '-' }}
                                @endif</td>
                              <td>{{ $barang->meja->ruangan->nama_ruangan }} {{ ' - ' }} {{ $barang->meja->nama_meja  }}</td>
                            </tr>
                            @endforeach
                            @endcan
                          </tbody>
                        </table>
            @endif
            <br>
        </div>
        <script type="text/javascript">
          window.print();
        </script>
       
    </div>         
    <script src="{{ asset('templates/assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('templates/assets/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('templates/assets/vendors/jquery-circle-progress/js/circle-progress.min.js') }}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('templates/assets/js/off-canvas.js') }}"></script>
    <script src="{{ asset('templates/assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('templates/assets/js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{ asset('templates/assets/js/dashboard.js') }}"></script>
    <!-- End custom js for this page -->
</body>
</html>