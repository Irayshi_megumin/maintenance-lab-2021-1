@extends('layouts.main')

@section('content')
<div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title">Aplikasi</h4>
                    <nav aria-label="breadcrumb" style="display: inline-block;">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Aplikasi</li>
                      </ol>
                    </nav>
                      <a href="/aplikasi/create">
                        <button type="button" class="btn btn-primary btn-icon-text" style="float: right;">
                        <i class="mdi mdi-application btn-icon-prepend"></i>	Add Aplikasi
                        </button>
                      </a>
                      @if($aplikasis->count() == null)
                      <h5>Tidak ada aplikasi, mohon tambahkan aplikasi!</h5>
                      @else
                      <table class="table table-hover mb-3">
                      <thead>
                        <tr>
                          <th> No </th>
                          <th> Nama </th>
                          <th> Action </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($aplikasis as $aplikasi)
                        <tr class="table">
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $aplikasi->nama_aplikasi }}</td>
                          <td> 
                          <!-- <button class="btn btn-outline-success">View <i class="mdi mdi-eye icon-large"></i></button> -->
                          <a href="/aplikasi/{{ $aplikasi->id }}/edit"><button class="btn btn-outline-primary">Edit <i class="mdi mdi-file-check icon-large"></i></button></a>
                          <form action="/aplikasi/{{ $aplikasi->id }}" method="post" style="display: inline-block;">
                              @method('delete')
                              @csrf
                              <button class="btn btn-outline-danger" type="submit" onclick="return confirm('Yakin ingin menghapus aplikasi ini dari semua komputer?')">Hapus <i class="mdi mdi-cup icon-large"></i></button>
                          </form>
                          </div>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $aplikasis->links() }}
                    @endif
                  </div>
                </div>
</div>
@endsection
