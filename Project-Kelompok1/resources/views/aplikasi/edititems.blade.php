@extends('layouts.main')

@section('content')
<div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Aplikasi</h4>
                    <form class="forms-sample" method="post" action="/aplikasi/{{ $aplikasi->id }}">
                      @method('put')
                      @csrf
                      <div class="form-group">
                        <label for="nama_aplikasi">Nama Aplikasi</label>
                        <input type="text" value="{{ old('nama_aplikasi', $aplikasi->nama_aplikasi) }}" name="nama_aplikasi" class="form-control @error('nama_aplikasi') is-invalid @enderror" id="nama_aplikasi" placeholder="Contoh: Ms Word" required>
                        @error('nama_aplikasi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                      </div>
                      <button type="submit" class="btn btn-primary float-right"><i class="mdi mdi-check btn-icon-prepend"></i> Submit</button>
                      <a href="/aplikasi" class="btn btn-danger float-right mr-2"><i class="mdi mdi-close btn-icon-prepend"></i> Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
@endsection