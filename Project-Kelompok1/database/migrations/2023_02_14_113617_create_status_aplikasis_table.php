<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_aplikasis', function (Blueprint $table) {
            $table->id();
            $table->foreignid('pc_id');
            $table->foreignid('aplikasi_id');
            $table->integer('status_aplikasi'); // (1 = belum, 2 = sudah)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_aplikasis');
    }
};
