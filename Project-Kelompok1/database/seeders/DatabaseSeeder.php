<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Meja;
use App\Models\User;
use App\Models\Ruangan;
use App\Models\Pengguna;
use App\Models\Barang;
use App\Models\Aplikasi;
use App\Models\StatusAplikasi;
use App\Models\Pc;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Pengguna::factory(10)->create();
        // User::factory(9)->create();

        Pengguna::create([
            'name' => 'Muhamad Raffi Irawan',
            'kelas_pengguna' => '12',
            'jurusan_pengguna' => 'RPL',
            'tlp_pengguna' => '+62 8781-1276-2459',
        ]);

        User::create([
            'username' => 'irayshi',
            'email' => 'irayshi@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('Admin'), // password
            'type_user' => '1',
            'remember_token' => Str::random(10),
            'pengguna_id' => '1'
        ]);

        for ($i = 1; $i < 4; $i++) {
            Ruangan::create([
                'nama_ruangan' => 'Ruang ' . $i
            ]);
        }

        for ($i = 1; $i < 11; $i++) {
            Meja::create([
                'nama_meja' => 'Meja ' . $i,
                'ruangan_id' => '1'
            ]);
        }

        for ($i = 1; $i < 11; $i++) {
            Barang::create([
                  'nama_barang' => 'Monitor',
                  'status_barang' => mt_rand(1, 3),
                  'meja_id' => $i
            ]);
        }

        for ($i = 1; $i < 11; $i++) {
            Barang::create([
                  'nama_barang' => 'Pc',
                  'status_barang' => mt_rand(1, 3),
                  'meja_id' => $i
            ]);
        }

        for ($i = 1; $i < 11; $i++) {
            Barang::create([
                  'nama_barang' => 'Keyboard',
                  'status_barang' => mt_rand(1, 3),
                  'meja_id' => $i
            ]);
        }

        for ($i = 1; $i < 11; $i++) {
            Barang::create([
                  'nama_barang' => 'Mouse',
                  'status_barang' => mt_rand(1, 3),
                  'meja_id' => $i
            ]);
        }

        for ($i = 11; $i < 21; $i++) {
            Pc::create([
                'kapasitas_ssd' => 'Atlon4 - 500GB',
                'kapasitas_hdd' => 'Sata2 - 128GB',
                'kapasitas_ram' => 'DDR3 - 12GB',
                'processor' => 'i7 core',
                'barang_id' => $i
            ]);
        }

        // for ($i = 1; $i < 3; $i++) {
        //     Aplikasi::create([
        //         'nama_aplikasi' => 'Aplikasi ' . $i
        //     ]);
        // }
        
        // for ($i = 1; $i < 11; $i++) {
        //     StatusAplikasi::create([
        //         'status_aplikasi' => mt_rand(1, 2),
        //         'aplikasi_id' => '1',
        //         'pc_id' => $i
        //     ]);
        // }
        // for ($i = 1; $i < 11; $i++) {
        //     StatusAplikasi::create([
        //         'status_aplikasi' => mt_rand(1, 2),
        //         'aplikasi_id' => '2',
        //         'pc_id' => $i
        //     ]);
        // }

        // for ($i = 91; $i < 181; $i++) {
        //     Aplikasi::create([
        //         'nama_aplikasi' => 'Excel',
        //         'status_aplikasi' => mt_rand(1, 2),
        //         'pc_id' => $i
        //     ]);
        // }

        // for ($i = 91; $i < 181; $i++) {
        //     Aplikasi::create([
        //         'nama_aplikasi' => 'Power Point',
        //         'status_aplikasi' => mt_rand(1, 2),
        //         'pc_id' => $i
        //     ]);
        // }
    }
}
