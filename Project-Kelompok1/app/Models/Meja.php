<?php

namespace App\Models;

use App\Models\Pengguna;
use App\Models\Ruangan;
use App\Models\Barang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Meja extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function pengguna() {
        return $this->hasMany(Pengguna::class);
    }

    public function barang() {
        return $this->hasMany(Barang::class);
    }

    public function ruangan() {
        return $this->belongsTo(Ruangan::class);
    }
}
