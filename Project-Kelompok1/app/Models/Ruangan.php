<?php

namespace App\Models;

use App\Models\Meja;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ruangan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function meja() {
        return $this->hasMany(Meja::class);
    }
}
