<?php

namespace App\Models;

use App\Models\Pc;
use App\Models\Meja;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function pc() {
        return $this->hasMany(Pc::class);
    }

    public function meja() {
        return $this->belongsTo(Meja::class);
    }
}
