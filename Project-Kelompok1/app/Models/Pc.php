<?php

namespace App\Models;

use App\Models\StatusAplikasi;
use App\Models\Barang;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pc extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function status_aplikasi() {
        return $this->hasMany(StatusAplikasi::class);
    }

    public function barang() {
        return $this->belongsTo(Barang::class);
    }
}
