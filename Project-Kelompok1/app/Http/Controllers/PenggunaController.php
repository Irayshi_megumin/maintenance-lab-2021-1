<?php

namespace App\Http\Controllers;

use App\Models\Pengguna;
use App\Models\Meja;
use App\Models\User;
use Illuminate\Http\Request;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Meja $meja)
    {
        return view('room.pengguna', [
            'meja' => $meja
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Meja $meja)
    {
        return view('room.addpengguna', [
            'meja_id' => $meja->id,
            'penggunas' => Pengguna::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'meja_id' => 'required'
        ]);

        Pengguna::where('id', $request->pengguna)->update($validatedData);

        return redirect('/pengguna/' . $request->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function show(Pengguna $pengguna)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengguna $pengguna)
    {
        return view('users.editusers', [
            'user' => $pengguna->user->first(),
            'mejas' => Meja::all(),
            'back' => '/pengguna/' . $pengguna->meja_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengguna $pengguna)
    {
        $rules1 = [
            'kelas_pengguna' => 'required|min:2|max:20',
            'jurusan_pengguna' => 'required|max:30',
        ];

        if ($request->name != $pengguna->name) {
            $rules1['name'] = 'required|min:3|max:30|unique:penggunas';
        }elseif ($request->tlp_pengguna != $pengguna->tlp_pengguna) {
            $rules1['tlp_pengguna'] = 'required|unique:penggunas';
        }

        $validatedData1 = $request->validate($rules1);
        
        $rules2 = [
            'type_user' => 'required'
        ];

        if ($request->username != $pengguna->user->first()->username) {
            $rules2['username'] = 'required|min:3|max:30|unique:users';
        } elseif ($request->email != $pengguna->user->first()->email) {
            $rules2['email'] = 'required|unique:users|email:dns';
        }

        $validatedData2 = $request->validate($rules2);
        
        Pengguna::where('id', $pengguna->id)->update($validatedData1);


        User::where('id', $pengguna->user->first()->id)->update($validatedData2);

        return redirect('/pengguna/' . $pengguna->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengguna  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengguna $pengguna)
    {
        Pengguna::where('id', $pengguna->id)->update([
            'meja_id' => null
        ]);
        return redirect('/pengguna/' . $pengguna->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
