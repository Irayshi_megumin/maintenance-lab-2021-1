<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.login', [
            'title' => 'Halaman Login'
        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if(Auth::attempt($credentials)) {
            $request->session()->regenerate();
            // $email = auth()->user()->email;
            // $data = [
            //     'title' => 'Login baru ke aplikasi Maintenance Lab',
            //     'email' => $email,
            //     'url' => 'http://127.0.0.1:8000',
            // ];
            // Mail::to($email)->send(new SendMail($data));
            return redirect()->intended('/ruangan');
        }

        return back()->with('loginError', 'Login Gagal');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}