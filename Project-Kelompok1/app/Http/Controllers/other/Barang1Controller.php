<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Ruangan;
use App\Models\Meja;
use App\Models\Pc;
use App\Models\Aplikasi;
use App\Models\StatusAplikasi;

class BarangController extends Controller
{
    public function index()
    {
        return view('barang.barang', [
            'barangs' => Barang::with(['meja'])->latest()->paginate(10)->withQueryString()
        ]);
    }
    
    public function addMonitor()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Monitor'
        ]);
    }
    public function addPc()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Pc'
        ]);
    }
    public function addKeyboard()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Keyboard'
        ]);
    }
    public function addMouse()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Mouse'
        ]);
    }

    public function editBarang(Barang $barang)
    {
        return view('barang.editbarang', [
            'mejas' => Meja::all(),
            'barang' => $barang
        ]);
    }

    public function store(Request $request) {
        $validatedData1 = $request->validate([
            'nama_barang' => 'required',
            'status_barang' => 'required',
            'meja_id' => 'required'
        ]);

        Barang::create($validatedData1);

        if ($validatedData1['nama_barang'] == 'Pc') {
            $validatedData2 = $request->validate([
                'kapasitas_ssd' => 'required',
                'kapasitas_hdd' => 'required',
                'kapasitas_ram' => 'required',
                'processor' => 'required',
            ]);
            $validatedData2['barang_id'] = Barang::where('nama_barang', $validatedData1['nama_barang'])->where('meja_id', $validatedData1['meja_id'])->first()->id;
            
            Pc::create($validatedData2);
            
            $pc_id = Pc::where('barang_id', $validatedData2['barang_id'])->first()->id;
            foreach (Aplikasi::all() as $aplikasi) {
                StatusAplikasi::create([
                    'status_aplikasi' => '1',
                    'aplikasi_id' => $aplikasi->id,
                    'pc_id' => $pc_id
                ]);
            }
        }
        return redirect('/barang')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function updateBarang(Request $request, Barang $barang)
    {
        $rules = $request->validate([
            'meja_id' => 'required',
            'status_barang' => 'required'
        ]);

        if ($barang->nama_barang == 'Pc') {
            $validatedData = $request->validate([
                'kapasitas_ssd' => 'required',
                'kapasitas_hdd' => 'required',
                'kapasitas_ram' => 'required',
                'processor' => 'required',
            ]);
            Pc::where('barang_id', $barang->id)->update($validatedData);
        }
        Barang::where('id', $barang->id)->update($rules);

        return redirect('/barang')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function delete(Barang $barang) {
        if ($barang->nama_barang == 'Pc') {
            foreach (StatusAplikasi::where('pc_id', $barang->pc->first()->id)->get() as $status_aplikasi) {
                StatusAplikasi::destroy($status_aplikasi->id);
            }
            Pc::destroy($barang->pc->first()->id);
        }
        Barang::destroy($barang->id);

        return redirect('/barang')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // public function simpan(Request $request)
    // {
    //     $meja = '2';
    //     return response()->json(['meja' => $meja]);
    // }
}
