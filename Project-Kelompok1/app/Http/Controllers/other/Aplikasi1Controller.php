<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aplikasi;
use App\Models\StatusAplikasi;
use App\Models\Pc;

class AplikasiController extends Controller
{
    public function index()
    {
        return view('aplikasi.aplikasi', [
            'aplikasis' => Aplikasi::all()
        ]);
    }

    public function additems()
    {
        return view('aplikasi.additems');
    }

    public function edit(Aplikasi $aplikasi)
    {
        return view('aplikasi.edititems', [
            'aplikasi' => $aplikasi
        ]);
    }

    public function store(Request $request) {
        
        $validatedData = $request->validate([
            'nama_aplikasi' => 'required|unique:aplikasis'
        ]);

        Aplikasi::create($validatedData);

        $aplikasi_id = Aplikasi::where('nama_aplikasi', $validatedData['nama_aplikasi'])->first()->id;

        foreach (Pc::all() as $pc) {
            StatusAplikasi::create([
                'status_aplikasi' => '1',
                'aplikasi_id' => $aplikasi_id,
                'pc_id' => $pc->id
            ]);
        }

        return redirect('/aplikasi')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function update(Request $request, Aplikasi $aplikasi) {
        $rules = [];

        if ($request->nama_aplikasi != $aplikasi->nama_aplikasi) {
            $rules['nama_aplikasi'] = 'required|unique:aplikasis';
        }

        $validatedData = $request->validate($rules);

        Aplikasi::where('id', $aplikasi->id)->update($validatedData);

        return redirect('/aplikasi')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function delete(Aplikasi $aplikasi) {
        foreach (StatusAplikasi::where('aplikasi_id', $aplikasi->id)->get() as $status) {
            StatusAplikasi::destroy($status->id);
        }
        Aplikasi::destroy($aplikasi->id);

        return redirect('/aplikasi')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
