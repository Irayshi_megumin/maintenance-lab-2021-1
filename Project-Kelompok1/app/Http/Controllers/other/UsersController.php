<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pengguna;
use App\Models\Meja;

class UsersController extends Controller
{
    public function index()
    {
        return view('users.users', [
            'users' => User::all()
        ]);
    }

    public function addusers()
    {
        return view('users.addusers', [
            'mejas' => Meja::all()
        ]);
    }

    public function edit(User $user)
    {
        return view('users.editusers', [
            'user' => $user
        ]);
    }
    
    public function store(Request $request) {
        
        $validatedData1 = $request->validate([
            'name' => 'required|min:3|max:30|unique:penggunas',
            'kelas_pengguna' => 'required|min:2|max:20',
            'jurusan_pengguna' => 'required|max:30',
            'tlp_pengguna' => 'required|unique:penggunas'
        ]);

        $validatedData2 = $request->validate([
            'username' => 'required|min:3|max:30|unique:users',
            'email' => 'required|unique:users|email:dns',
            'password' => 'required|min:5|max:30',
            'type_user' => 'required'
        ]);
        
        $validatedData1['meja_id'] = $request->meja_id;
        
        Pengguna::create($validatedData1);

        $validatedData2['pengguna_id'] = Pengguna::where('name', $validatedData1['name'])->first()->id;
        
        $validatedData2['password'] = bcrypt($validatedData2['password']);

        User::create($validatedData2);

        return redirect('/users')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
    
    public function delete(User $user) {
        Pengguna::destroy($user->pengguna->id);
        User::destroy($user->id);
        return redirect('/users')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
