<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ruangan;
use App\Models\Pengguna;
use App\Models\Barang;
use App\Models\User;
use App\Models\Meja;
use App\Models\Pc;
use App\Models\Aplikasi;
use App\Models\StatusAplikasi;

class RoomController extends Controller
{
    public function index()
    {
        return view('room.room', [
            'rooms' => Ruangan::all()
        ]);
    }

    public function addroom()
    {
        return view('room.addroom');
    }

    public function editroom()
    {
        return view('room.editroom');
    }

    public function meja(Ruangan $ruangan)
    {
        return view('room.meja', [
            'mejas' => $ruangan->meja,
            'ruangan' => $ruangan
        ]);
    }

    public function addmeja(Ruangan $ruangan)
    {
        return view('room.addmeja', [
            'ruangan' => $ruangan
        ]);
    }

    public function editmeja(Meja $meja)
    {
        return view('room.editmeja', [
            'meja' => $meja
        ]);
    }

    public function updatemeja(Request $request, Meja $meja)
    {
        $validatedData = [];
        if ($request->nama_meja != $meja->nama_meja) {
            if ($meja->where('nama_meja', $request->nama_meja)->where('ruangan_id', $meja->ruangan_id)->count()) {
                $validatedData = $request->validate([
                    'nama_meja' => 'required|unique:mejas'
                ]);
            }
            $validatedData = $request->validate([
                'nama_meja' => 'required'
            ]);
        }
        
        Meja::where('id', $meja->id)->update($validatedData);
        return redirect('/room/' . $meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');

    }

    public function createmeja(Request $request, Ruangan $ruangan)
    {
        $validatedData = [
            'ruangan_id' => $ruangan->id
        ];

        if ($ruangan->meja->where('nama_meja', $request->nama_meja)->count()) {
            $validatedData['nama_meja'] = $request->validate([
                'nama_meja' => 'required|unique:mejas'
            ]);
        } else {
            $validatedData['nama_meja'] = $request->nama_meja;
            Meja::create($validatedData);
            return redirect('/room/' . $ruangan->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
        }
    }

    public function pengguna(Meja $meja)
    {
        return view('room.pengguna', [
            'mejas' => $meja
        ]);
    }

    public function addpengguna(Meja $meja)
    {
        return view('room.addpengguna', [
            'meja_id' => $meja->id,
            'penggunas' => Pengguna::all()
        ]);
    }

    public function update_pengguna(Request $request) {
        $validatedData = $request->validate([
            'meja_id' => 'required'
        ]);

        Pengguna::where('id', $request->pengguna)->update($validatedData);

        return redirect('/pengguna/' . $request->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function editpengguna(Pengguna $pengguna)
    {
        return view('room.editpengguna', [
            'pengguna' => $pengguna
        ]);
    }

    public function edit_proses_pengguna(Request $request, Pengguna $pengguna) {
        
        $rules1 = [
            'kelas_pengguna' => 'required|min:2|max:20',
            'jurusan_pengguna' => 'required|max:30',
        ];

        if ($request->name != $pengguna->name) {
            $rules1['name'] = 'required|min:3|max:30|unique:penggunas';
        }elseif ($request->tlp_pengguna != $pengguna->tlp_pengguna) {
            $rules1['tlp_pengguna'] = 'required|unique:penggunas';
        }

        $validatedData1 = $request->validate($rules1);
        
        $rules2 = [
            'type_user' => 'required'
        ];

        if ($request->username != $pengguna->user->first()->username) {
            $rules2['username'] = 'required|min:3|max:30|unique:users';
        } elseif ($request->email != $pengguna->user->first()->email) {
            $rules2['email'] = 'required|unique:users|email:dns';
        }

        $validatedData2 = $request->validate($rules2);
        
        Pengguna::where('id', $pengguna->id)->update($validatedData1);


        User::where('id', $pengguna->user->first()->id)->update($validatedData2);

        return redirect('/pengguna/' . $pengguna->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function delete_pengguna(Pengguna $pengguna)
    {
        $delete = [
            'meja_id' => null
        ];

        Pengguna::where('id', $pengguna->id)->update($delete);
        return redirect('/pengguna/' . $pengguna->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function monitor(Meja $meja)
    {
        return view('room.editmonitor', [
            'monitor' => $meja->barang->where('nama_barang', 'Monitor')->first(),
            'meja' => $meja
        ]);
    }

    public function createmonitor(Meja $meja)
    {
        Barang::create([
            'nama_barang' => 'Monitor',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ]);
        return redirect('/monitor/' . $meja->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function updateBarang(Request $request, Barang $barang)
    {
        Barang::where('id', $barang->id)->update([
            'status_barang' => $request->status_barang
        ]);
        return redirect('/room/' . $barang->meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function pc(Meja $meja)
    {
        return view('room.editpc', [
            'barang' => $meja->barang->where('nama_barang', 'Pc')->first(),
            'meja' => $meja
        ]);
    }

    public function createpc(Request $request, Meja $meja)
    {
        $validatedData1 = [
            'nama_barang' => 'Pc',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ];

        Barang::create($validatedData1);

        $validatedData2 = $request->validate([
            'kapasitas_ssd' => 'required',
            'kapasitas_hdd' => 'required',
            'kapasitas_ram' => 'required',
            'processor' => 'required',
        ]);
        $validatedData2['barang_id'] = Barang::where('nama_barang', $validatedData1['nama_barang'])->where('meja_id', $validatedData1['meja_id'])->first()->id;
        
        Pc::create($validatedData2);
        $pc_id = Pc::where('barang_id', $validatedData2['barang_id'])->first()->id;
        foreach (Aplikasi::all() as $aplikasi) {
            StatusAplikasi::create([
                'status_aplikasi' => '1',
                'aplikasi_id' => $aplikasi->id,
                'pc_id' => $pc_id
            ]);
        }
        return redirect('/room/' . $meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function updatepc(Request $request, Barang $barang)
    {
        $validatedData = $request->validate([
            'kapasitas_ssd' => 'required',
            'kapasitas_hdd' => 'required',
            'kapasitas_ram' => 'required',
            'processor' => 'required',
        ]);
        Barang::where('id', $barang->id)->update([
            'status_barang' => $request->status_barang
        ]);
        Pc::where('barang_id', $barang->id)->update($validatedData);
        return redirect('/room/' . $barang->meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function mouse(Meja $meja)
    {
        return view('room.editmouse', [
            'mouse' => $meja->barang->where('nama_barang', 'Mouse')->first(),
            'meja' => $meja
        ]);
    }

    public function createmouse(Meja $meja)
    {
        Barang::create([
            'nama_barang' => 'Mouse',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ]);
        return redirect('/mouse/' . $meja->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function keyboard(Meja $meja)
    {
        return view('room.editkeyboard', [
            'keyboard' => $meja->barang->where('nama_barang', 'Keyboard')->first(),
            'meja' => $meja
        ]);
    }

    public function createkeyboard(Meja $meja)
    {
        Barang::create([
            'nama_barang' => 'Keyboard',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ]);
        return redirect('/keyboard/' . $meja->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function aplikasion(Meja $meja)
    {
        return view('room.aplikasion', [
            'aplikasis' => APlikasi::all(),
            'status_aplikasis' => $meja->barang->where('nama_barang', 'Pc')->first()->pc->first()->status_aplikasi
        ]);
    }

    public function updateaplikasion(Request $request, Pc $pc)
    {
        $i = 0;
        foreach ($request->aplikasi_id as $aplikasi_id) {
            StatusAplikasi::where('pc_id', $pc->id)->where('aplikasi_id', $aplikasi_id)->update(['status_aplikasi' => $request->status_aplikasi[$i]]);
            $i++;
        }
        return redirect('/room/' . $pc->barang->meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
