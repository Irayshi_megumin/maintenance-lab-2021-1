<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    public function index()
    {
        return view('transaksi.new');
    }

    public function borrowed()
    {
        return view('transaksi.borrowed');
    }

    public function returned()
    {
        return view('transaksi.returned');
    }
}
