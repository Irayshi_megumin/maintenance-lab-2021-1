<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index() {
        return view('register.register', [
            'title' => 'Halaman Register'
        ]);
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|min:3|max:30',
            'username' => 'required|min:3|max:30|unique:users',
            'email' => 'required|unique:users|email:dns',
            'password' => 'required|min:5|max:30'
        ]);

        $validatedData['password'] = bcrypt($validatedData['password']);

        User::create($validatedData);

        // $request->session()->flash('success', 'Pendaftaran berhasil, mohon untuk login!');

        return redirect('/login')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
