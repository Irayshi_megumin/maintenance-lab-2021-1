<?php

namespace App\Http\Controllers;

use App\Models\Aplikasi;
use App\Models\StatusAplikasi;
use App\Models\Pc;
use Illuminate\Http\Request;

class AplikasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('aplikasi.aplikasi', [
            'aplikasis' => Aplikasi::latest()->paginate(10)->withQueryString()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aplikasi.additems');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_aplikasi' => 'required|unique:aplikasis'
        ]);

        Aplikasi::create($validatedData);

        $aplikasi_id = Aplikasi::where('nama_aplikasi', $validatedData['nama_aplikasi'])->first()->id;

        foreach (Pc::all() as $pc) {
            StatusAplikasi::create([
                'status_aplikasi' => '1',
                'aplikasi_id' => $aplikasi_id,
                'pc_id' => $pc->id
            ]);
        }

        return redirect('/aplikasi')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aplikasi  $aplikasi
     * @return \Illuminate\Http\Response
     */
    public function show(Aplikasi $aplikasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Aplikasi  $aplikasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Aplikasi $aplikasi)
    {
        return view('aplikasi.edititems', [
            'aplikasi' => $aplikasi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Aplikasi  $aplikasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aplikasi $aplikasi)
    {
        $rules = [];

        if ($request->nama_aplikasi != $aplikasi->nama_aplikasi) {
            $rules['nama_aplikasi'] = 'required|unique:aplikasis';
        }

        $validatedData = $request->validate($rules);

        Aplikasi::where('id', $aplikasi->id)->update($validatedData);

        return redirect('/aplikasi')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aplikasi  $aplikasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aplikasi $aplikasi)
    {
        foreach (StatusAplikasi::where('aplikasi_id', $aplikasi->id)->get() as $status) {
            StatusAplikasi::destroy($status->id);
        }
        Aplikasi::destroy($aplikasi->id);

        return redirect('/aplikasi')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
