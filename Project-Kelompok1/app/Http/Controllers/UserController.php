<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Pengguna;
use App\Models\Meja;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.users', [
            'users' => User::latest()->paginate(10)->withQueryString(),
        ]);
    }

    public function cetak()
    {
        return view('users.cetak', [
            'users' => User::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.addusers', [
            'mejas' => Meja::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData1 = $request->validate([
            'name' => 'required|min:3|max:30|unique:penggunas',
            'kelas_pengguna' => 'required|min:2|max:20',
            'jurusan_pengguna' => 'required|max:30',
            'tlp_pengguna' => 'required|unique:penggunas'
        ]);

        $validatedData2 = $request->validate([
            'username' => 'required|min:3|max:30|unique:users',
            'email' => 'required|unique:users|email:dns',
            'password' => 'required|min:5|max:30',
            'type_user' => 'required'
        ]);
        
        $validatedData1['meja_id'] = $request->meja_id;
        
        Pengguna::create($validatedData1);

        $validatedData2['pengguna_id'] = Pengguna::where('name', $validatedData1['name'])->first()->id;
        
        $validatedData2['password'] = bcrypt($validatedData2['password']);

        User::create($validatedData2);

        return redirect('/user')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.editusers', [
            'user' => $user,
            'mejas' => Meja::all(),
            'back' => '/user'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules1 = [
            'kelas_pengguna' => 'required|min:2|max:20',
            'jurusan_pengguna' => 'required|max:30',
        ];

        if ($request->name != $user->pengguna->name) {
            $rules1['name'] = 'required|min:3|max:30|unique:penggunas';
        }elseif ($request->tlp_pengguna != $user->pengguna->tlp_pengguna) {
            $rules1['tlp_pengguna'] = 'required|unique:penggunas';
        }

        $validatedData1 = $request->validate($rules1);
        
        $validatedData1['meja_id'] = $request->meja_id;
        
        $rules2 = [
            'type_user' => 'required'
        ];

        if ($request->username != $user->username) {
            $rules2['username'] = 'required|min:3|max:30|unique:users';
        } elseif ($request->email != $user->email) {
            $rules2['email'] = 'required|unique:users|email:dns';
        }

        $validatedData2 = $request->validate($rules2);
        
        Pengguna::where('id', $user->pengguna->id)->update($validatedData1);


        User::where('id', $user->id)->update($validatedData2);

        return redirect('/user')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        Pengguna::destroy($user->pengguna->id);
        User::destroy($user->id);
        return redirect('/user')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
