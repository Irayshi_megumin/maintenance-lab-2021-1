<?php

namespace App\Http\Controllers;

use App\Models\Ruangan;
use App\Models\Meja;
use App\Models\Barang;
use App\Models\Pc;
use App\Models\StatusAplikasi;
use Illuminate\Http\Request;

class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('room.room', [
            'rooms' => Ruangan::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('room.addroom');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_ruangan' => 'required|unique:ruangans'
        ]); 
        
        Ruangan::create($validatedData);  

        return redirect('/ruangan')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function show(Ruangan $ruangan)
    {
        return view('room.meja', [
            'mejas' => $ruangan->meja,
            'ruangan' => $ruangan
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function edit(Ruangan $ruangan)
    {
        return view('room.editroom', [
            'ruangan' => $ruangan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ruangan $ruangan)
    {
        $rules = [];

        if ($request->nama_ruangan != $ruangan->nama_ruangan) {
            $rules['nama_ruangan'] = 'required|unique:ruangans';
        }

        $validatedData = $request->validate($rules);
        

        Ruangan::where('id', $ruangan->id)->update($validatedData);

        return redirect('/ruangan')->with('success', 'Pendaftaran berhasil, mohon untuk login!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ruangan  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ruangan $ruangan)
    {
        foreach ($ruangan->meja as $meja) {
            foreach ($meja->barang as $barang) {
                foreach ($barang->pc as $pc) {
                    foreach ($pc->status_aplikasi as $status_aplikasi) {
                        StatusAplikasi::destroy($status_aplikasi->id);
                    }
                    Pc::destroy($pc->id);
                }
                Barang::destroy($barang->id);
            }
            Meja::destroy($meja->id);
        }
        Ruangan::destroy($ruangan->id);
        return redirect('/ruangan')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
