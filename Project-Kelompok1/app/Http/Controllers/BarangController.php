<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Pc;
use App\Models\Meja;
use App\Models\Aplikasi;
use App\Models\StatusAplikasi;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('barang.barang', [
            'barangs' => Barang::with(['meja'])->latest()->paginate(10)->withQueryString(),
            'monitors' => Barang::with(['meja'])->latest()->where('nama_barang', 'Monitor')->get(),
            'pcs' => Barang::with(['meja'])->latest()->where('nama_barang', 'Pc')->get(),
            'mouses' => Barang::with(['meja'])->latest()->where('nama_barang', 'Mouse')->get(),
            'keyboards' => Barang::with(['meja'])->latest()->where('nama_barang', 'Keyboard')->get()
        ]);
    }

    public function cetak()
    {
        return view('barang.cetak', [
            'barangs' => Barang::with(['meja'])->latest()->paginate(10)->withQueryString(),
            'monitors' => Barang::with(['meja'])->latest()->where('nama_barang', 'Monitor')->get(),
            'pcs' => Barang::with(['meja'])->latest()->where('nama_barang', 'Pc')->get(),
            'mouses' => Barang::with(['meja'])->latest()->where('nama_barang', 'Mouse')->get(),
            'keyboards' => Barang::with(['meja'])->latest()->where('nama_barang', 'Keyboard')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Monitor()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Monitor'
        ]);
    }
    public function Pc()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Pc'
        ]);
    }
    public function Keyboard()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Keyboard'
        ]);
    }
    public function Mouse()
    {
        return view('barang.addbarang', [
            'mejas' => Meja::all(),
            'barangs' => 'Mouse'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData1 = $request->validate([
            'nama_barang' => 'required',
            'status_barang' => 'required',
            'meja_id' => 'required'
        ]);

        Barang::create($validatedData1);

        if ($validatedData1['nama_barang'] == 'Pc') {
            $validatedData2 = $request->validate([
                'kapasitas_ssd' => 'required',
                'kapasitas_hdd' => 'required',
                'kapasitas_ram' => 'required',
                'processor' => 'required',
            ]);
            $validatedData2['barang_id'] = Barang::where('nama_barang', $validatedData1['nama_barang'])->where('meja_id', $validatedData1['meja_id'])->first()->id;
            
            Pc::create($validatedData2);
            
            $pc_id = Pc::where('barang_id', $validatedData2['barang_id'])->first()->id;
            foreach (Aplikasi::all() as $aplikasi) {
                StatusAplikasi::create([
                    'status_aplikasi' => '1',
                    'aplikasi_id' => $aplikasi->id,
                    'pc_id' => $pc_id
                ]);
            }
        }
        return redirect('/barang')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        return view('barang.editbarang', [
            'mejas' => Meja::all(),
            'barang' => $barang
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {
        $rules = $request->validate([
            'status_barang' => 'required'
        ]);

        if ($barang->nama_barang == 'Pc') {
            $validatedData = $request->validate([
                'kapasitas_ssd' => 'required',
                'kapasitas_hdd' => 'required',
                'kapasitas_ram' => 'required',
                'processor' => 'required',
            ]);
            Pc::where('barang_id', $barang->id)->update($validatedData);
        }
        
        if ($request->meja_id){
            $rules['meja_id'] = $request->meja_id;
        }

        Barang::where('id', $barang->id)->update($rules);

        return redirect('/barang')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        if ($barang->nama_barang == 'Pc') {
            foreach (StatusAplikasi::where('pc_id', $barang->pc->first()->id)->get() as $status_aplikasi) {
                StatusAplikasi::destroy($status_aplikasi->id);
            }
            Pc::destroy($barang->pc->first()->id);
        }
        Barang::destroy($barang->id);

        return redirect('/barang')->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }
}
