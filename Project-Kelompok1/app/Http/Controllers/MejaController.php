<?php

namespace App\Http\Controllers;

use App\Models\Meja;
use App\Models\Ruangan;
use App\Models\Barang;
use App\Models\Pc;
use App\Models\Aplikasi;
use App\Models\StatusAplikasi;
use Illuminate\Http\Request;

class MejaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Ruangan $ruangan)
    {
        return view('room.addmeja', [
            'ruangan' => $ruangan
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Ruangan $ruangan)
    {
        $validatedData = [
            'ruangan_id' => $ruangan->id
        ];

        if ($ruangan->meja->where('nama_meja', $request->nama_meja)->count()) {
            $validatedData['nama_meja'] = $request->validate([
                'nama_meja' => 'required|unique:mejas'
            ]);
        } else {
            $validatedData['nama_meja'] = $request->nama_meja;
            Meja::create($validatedData);
            return redirect('/ruangan/' . $ruangan->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Meja  $meja
     * @return \Illuminate\Http\Response
     */
    public function edit(Meja $meja)
    {
        return view('room.editmeja', [
            'meja' => $meja
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Meja  $meja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meja $meja)
    {
        $validatedData = [];
        if ($request->nama_meja != $meja->nama_meja) {
            if ($meja->where('nama_meja', $request->nama_meja)->where('ruangan_id', $meja->ruangan_id)->count()) {
                $validatedData = $request->validate([
                    'nama_meja' => 'required|unique:mejas'
                ]);
            }
            $validatedData = $request->validate([
                'nama_meja' => 'required'
            ]);
        }
        
        Meja::where('id', $meja->id)->update($validatedData);
        return redirect('/ruangan/' . $meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meja  $ruangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meja $meja)
    {
        foreach ($meja->barang as $barang) {
            foreach ($barang->pc as $pc) {
                foreach ($pc->status_aplikasi as $status_aplikasi) {
                    StatusAplikasi::destroy($status_aplikasi->id);
                }
                Pc::destroy($pc->id);
            }
            Barang::destroy($barang->id);
        }
        Meja::destroy($meja->id);
        return redirect('/ruangan/' . $meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // Update Barang
    public function updateBarang(Request $request, Barang $barang)
    {
        Barang::where('id', $barang->id)->update([
            'status_barang' => $request->status_barang
        ]);
        return redirect('/ruangan/' . $barang->meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // Resource Monitor
    public function monitor(Meja $meja)
    {
        return view('room.editmonitor', [
            'monitor' => $meja->barang->where('nama_barang', 'Monitor')->first(),
            'meja' => $meja
        ]);
    }

    public function createmonitor(Meja $meja)
    {
        Barang::create([
            'nama_barang' => 'Monitor',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ]);
        return redirect('/monitor/' . $meja->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // Resource Pc
    public function pc(Meja $meja)
    {
        return view('room.editpc', [
            'pc' => $meja->barang->where('nama_barang', 'Pc')->first(),
            'meja' => $meja
        ]);
    }

    public function createpc(Request $request, Meja $meja)
    {
        $validatedData1 = [
            'nama_barang' => 'Pc',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ];

        Barang::create($validatedData1);

        $validatedData2 = $request->validate([
            'kapasitas_ssd' => 'required',
            'kapasitas_hdd' => 'required',
            'kapasitas_ram' => 'required',
            'processor' => 'required',
        ]);
        $validatedData2['barang_id'] = Barang::where('nama_barang', $validatedData1['nama_barang'])->where('meja_id', $validatedData1['meja_id'])->first()->id;
        
        Pc::create($validatedData2);
        $pc_id = Pc::where('barang_id', $validatedData2['barang_id'])->first()->id;
        foreach (Aplikasi::all() as $aplikasi) {
            StatusAplikasi::create([
                'status_aplikasi' => '1',
                'aplikasi_id' => $aplikasi->id,
                'pc_id' => $pc_id
            ]);
        }
        return redirect('/ruangan/' . $meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    public function updatepc(Request $request, Barang $barang)
    {
        $validatedData = $request->validate([
            'kapasitas_ssd' => 'required',
            'kapasitas_hdd' => 'required',
            'kapasitas_ram' => 'required',
            'processor' => 'required',
        ]);
        Barang::where('id', $barang->id)->update([
            'status_barang' => $request->status_barang
        ]);
        Pc::where('barang_id', $barang->id)->update($validatedData);
        return redirect('/ruangan/' . $barang->meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // Resource Mouse
    public function mouse(Meja $meja)
    {
        return view('room.editmouse', [
            'mouse' => $meja->barang->where('nama_barang', 'Mouse')->first(),
            'meja' => $meja
        ]);
    }

    public function createmouse(Meja $meja)
    {
        Barang::create([
            'nama_barang' => 'Mouse',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ]);
        return redirect('/mouse/' . $meja->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // Resource Keyboard
    public function keyboard(Meja $meja)
    {
        return view('room.editkeyboard', [
            'keyboard' => $meja->barang->where('nama_barang', 'Keyboard')->first(),
            'meja' => $meja
        ]);
    }

    public function createkeyboard(Meja $meja)
    {
        Barang::create([
            'nama_barang' => 'Keyboard',
            'status_barang' => '1',
            'meja_id' => $meja->id
        ]);
        return redirect('/keyboard/' . $meja->id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
    }

    // Resource Aplikasi
    public function aplikasion(Meja $meja)
    {
        return view('room.aplikasion', [
            'aplikasis' => APlikasi::all(),
            'status_aplikasis' => $meja->barang->where('nama_barang', 'Pc')->first()->pc->first()->status_aplikasi,
            'back' => $meja->ruangan->id
        ]);
    }

    public function updateaplikasion(Request $request, Pc $pc)
    {
        $i = 0;
        foreach ($request->aplikasi_id as $aplikasi_id) {
            StatusAplikasi::where('pc_id', $pc->id)->where('aplikasi_id', $aplikasi_id)->update(['status_aplikasi' => $request->status_aplikasi[$i]]);
            $i++;
        }
        if (auth()->user()->type_user == '1') {
            return redirect('/ruangan/' . $pc->barang->meja->ruangan_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
        }else {
            return redirect('/aplikasion/' . $pc->barang->meja_id)->with('success', 'Pendaftaran berhasil, mohon untuk login!');
        }
    }
}
